@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">
            @include('analyses.keywords.partials.schedule-web-page-box')
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            @include('analyses.recently-completed-box')
        </div>
        <div class="col-md-6">
            @include('jobs.recently-failed-box')
        </div>
    </div>
@stop

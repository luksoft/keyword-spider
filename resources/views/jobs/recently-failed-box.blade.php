<div class="box box-danger">
    <div class="box-header">
        <h3 class="box-title">@lang('Recently failed jobs')</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding table-responsive">
        <table class="table table-condensed">
            <thead>
            <tr>
                <th class="col-md-2">@lang('Description')</th>
                <th class="col-md-6">@lang('Error')</th>
                <th class="col-md-6">@lang('When')</th>
            </tr>
            </thead>
            <tbody>
            @php
            /** @var \App\Jobs\FailedJob $failedJob */
            @endphp
            @forelse($failedJobs as $failedJob)
                <tr>
                    <td>{{$failedJob->description()}}</td>
                    <td>{{$failedJob->errorMessage()}}</td>
                    <td title="{{$failedJob->created()->toDateTimeString()}}">{{$failedJob->created()->diffForHumans()}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="2">
                        @lang('No failed jobs has been found')
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('Schedule web page keyword analysis')</h3>
    </div>
    {!! BootForm::open(['route' => 'analyses.keywords.web-page-document.create']) !!}
    <div class="box-body">
        {!! BootForm::url('url', false, null, ['class' => 'form-control input-lg', 'placeholder' => trans('Enter page address')]) !!}
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary">@lang('Submit')</button>
    </div>
    {!! BootForm::close() !!}
</div>
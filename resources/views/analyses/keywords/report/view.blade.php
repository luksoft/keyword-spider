@extends('adminlte::page')

@section('title', 'Document keyword analysis')

@php
/** @var \App\Analyses\DocumentAnalysis $analysis */
@endphp
@section('content_header')
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{$analysis->document()->name()}} &ndash; @lang('Keywords occurrences') </h3>
                    <div class="box-tools pull-right"><a href="{{route('home')}}">@lang('Return to dashboard')</a></div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding table-responsive">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <th class="col-md-8">
                                {{$analysis->report()->header()[\App\Analyses\KeywordsDocumentAnalysis::COLUMN_KEYWORD]}}
                            </th>
                            <th>
                                {{$analysis->report()->header()[\App\Analyses\KeywordsDocumentAnalysis::COLUMN_OCCURRENCES]}}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($analysis->report()->data() as $keywordData)
                            <tr>
                                <td>
                                    {{$keywordData[\App\Analyses\KeywordsDocumentAnalysis::COLUMN_KEYWORD]}}
                                </td>
                                <td>
                                    {{$keywordData[\App\Analyses\KeywordsDocumentAnalysis::COLUMN_OCCURRENCES]}}
                                </td>
                            </tr>
                        @empty
                            <tr><td colspan="2">@lang('No keywords were found in the document')</td></tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

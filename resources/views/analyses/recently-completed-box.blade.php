<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">@lang('Recently completed analyses')</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding table-responsive">
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>#</th>
                <th class="col-md-2">@lang('Type')</th>
                <th class="col-md-5">@lang('Subject')</th>
                <th class="col-md-2">@lang('Created')</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @php
            /** @var \App\Analyses\DocumentAnalysis $analysis */
            @endphp
            @forelse($analyses as $analysis)
                <tr>
                    <td>{{($loop->index+1)}}</td>
                    <td>{{$analysis->type()}}</td>
                    <td>{{$analysis->document()->name()}}</td>
                    <td title="{{$analysis->created()->toDateTimeString()}}">{{$analysis->created()->diffForHumans()}}</td>
                    <td>
                        <a href="{{route('analyses.'.$analysis->type().'.report.view', $analysis->uuid()->toString())}}">
                            @lang('show results')
                        </a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="2">
                        @lang('No analyses has been completed yet')
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>

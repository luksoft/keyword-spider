<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index')->name('home');

Route::post(
    '/analyses/keywords/web-page-document',
    'Analyses\Keywords\WebPageDocumentController@create'
)->name('analyses.keywords.web-page-document.create');

Route::get(
    '/analyses/keywords/report/{id}',
    'Analyses\Keywords\ReportController@view'
)->name('analyses.keywords.report.view');

Auth::routes();


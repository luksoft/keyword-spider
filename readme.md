## Test App

### Requirements
* PHP 7.2

### Installation
* install Composer from `https://getcomposer.org/download/`
* copy `.env.example` to `.env` 
* run `php composer.phar install` - to install all dependencies
* run `php artisan key:generate`
* run `php artisan serve` - to start local web server on port 8000

### Usage
* entrypoint for application - `http://127.0.0.1:8000/`
* entrypoint for ananlysis - `app/Http/Controllers/Analyses/Keywords/WebPageDocumentController.php`

### HLA
1. retrieve web page
2. normalize web page - create text document (encode to UTF-8, get keywords, extract text from html etc.)
3. analyse text document

### TODO
* test coverage: unit, integration
* proper error handling
* language detection
* text stemming

<?php namespace App\Analyses\Repository;

use App\Analyses\DocumentAnalysis;
use App\Storage\FixedCircularMap;
use Illuminate\Cache\Repository;

/**
 * Simple implementation using cache storage, not safe for concurrent usage.
 * It stores fixed number of analysis, oldest are lost if the buffer is full.
 * Do not use in production!!!
 */
class CacheAnalysesRepository implements AnalysesRepository
{
    const COLLECTION_KEY = 'analyses-collection';
    const COLLECTION_SIZE = 20;

    /** @var Repository */
    private $storage;

    /** @var FixedCircularMap */
    private $collection;

    /** @var int */
    private $size;

    public function __construct(Repository $storage, int $size = self::COLLECTION_SIZE)
    {
        $this->storage = $storage;
        $this->size = $size;
    }

    public function store(DocumentAnalysis $analysis)
    {
        $this->loadCollection();

        $uuid = $analysis->uuid()->toString();

        $this->collection->set($uuid, $analysis);

        $this->storeCollection();
    }

    public function all(): \Generator
    {
        $this->loadCollection();

        return $this->collection->all();
    }

    public function find(string $id): ?DocumentAnalysis
    {
        $this->loadCollection();

        return $this->collection->get($id);
    }

    private function loadCollection()
    {
        $this->collection = $this->storage->get(
            static::COLLECTION_KEY,
            function () {
                return new FixedCircularMap($this->size);
            }
        );
    }

    private function storeCollection()
    {
        $this->storage->forever(static::COLLECTION_KEY, $this->collection);
    }
}

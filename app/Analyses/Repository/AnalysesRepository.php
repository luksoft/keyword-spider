<?php namespace App\Analyses\Repository;

use App\Analyses\DocumentAnalysis;

interface AnalysesRepository
{
    public function store(DocumentAnalysis $analysis);
    public function all(): \Generator;
    public function find(string $id): ?DocumentAnalysis;
}

<?php namespace App\Analyses;

use App\Document\Document;
use App\Report\Report;
use Carbon\Carbon;
use Ramsey\Uuid\UuidInterface;

interface DocumentAnalysis
{
    public function uuid(): UuidInterface;
    public function type(): string;
    public function document(): Document;
    public function report(): Report;
    public function created(): Carbon;
}

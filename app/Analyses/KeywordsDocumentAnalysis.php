<?php namespace App\Analyses;

use App\Document\Document;
use App\Report\Report;
use Carbon\Carbon;
use Ramsey\Uuid\UuidInterface;

class KeywordsDocumentAnalysis implements DocumentAnalysis
{
    const COLUMN_KEYWORD = 'keyword';
    const COLUMN_OCCURRENCES = 'occurrences';

    /** @var UuidInterface */
    private $uuid;

    /** @var Document */
    private $document;

    /** @var Report */
    private $report;

    /** @var Carbon */
    private $created;

    public function __construct(UuidInterface $uuid, Document $document, Report $report)
    {
        $this->uuid = $uuid;
        $this->document = $document;
        $this->report = $report;
        $this->created = Carbon::now();
    }

    public function uuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function type(): string
    {
        return AnalysisType::KEYWORDS;
    }

    public function document(): Document
    {
        return $this->document;
    }

    public function report(): Report
    {
        return $this->report;
    }

    public function created(): Carbon
    {
        return $this->created;
    }
}

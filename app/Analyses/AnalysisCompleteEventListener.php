<?php

namespace App\Analyses;

use App\Analyses\Repository\AnalysesRepository;
use App\Analyzers\AnalysisCompletedEvent;

class AnalysisCompleteEventListener
{
    /** @var AnalysesRepository */
    private $analysesRepository;

    public function __construct(AnalysesRepository $analysesRepository)
    {
        $this->analysesRepository = $analysesRepository;
    }

    public function handle(AnalysisCompletedEvent $event)
    {
        $this->analysesRepository->store($event->analysis());
    }
}

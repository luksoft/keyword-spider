<?php namespace App\Http\Controllers\Analyses\Keywords;

use App\Analyses\Repository\AnalysesRepository;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function view(AnalysesRepository $analysesRepository, $id)
    {
        $analysis = $analysesRepository->find($id);

        if (is_null($analysis)) {
            abort(404);
        }

        return view('analyses.keywords.report.view', compact('analysis'));
    }
}

<?php namespace App\Http\Controllers\Analyses\Keywords;

use App\Analyses\AnalysisType;
use App\HtmlDocument\AnalyseWebPageJob;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ramsey\Uuid\UuidFactoryInterface;

class WebPageDocumentController extends Controller
{
    /**
     * @param UuidFactoryInterface $uuidFactory
     * @param Request              $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(UuidFactoryInterface $uuidFactory, Request $request)
    {
        $jobId = $uuidFactory->uuid1();

        $job = new AnalyseWebPageJob($jobId, $request->get('url'), AnalysisType::KEYWORDS);

        $this->dispatch($job);

        $this->flash(trans('Web page keyword analysis has been scheduled. Please check the results later.'));

        return redirect()->back();
    }
}

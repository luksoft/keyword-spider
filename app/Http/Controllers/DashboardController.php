<?php namespace App\Http\Controllers;

use App\Analyses\Repository\AnalysesRepository;
use App\Jobs\Repository\FailedJobsRepository;

class DashboardController extends Controller
{
    public function index(AnalysesRepository $analysesRepository, FailedJobsRepository $failedJobsRepository)
    {
        return view('dashboard.index', [
            'analyses' => $analysesRepository->all(),
            'failedJobs' => $failedJobsRepository->all(),
        ]);
    }
}

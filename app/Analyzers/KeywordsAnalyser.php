<?php namespace App\Analyzers;

use App\Analyses\DocumentAnalysis;
use App\Analyses\KeywordsDocumentAnalysis;
use App\Document\Document;
use App\Report\Report;
use App\Tokenizers\Tokenizer;
use Ramsey\Uuid\UuidFactoryInterface;

class KeywordsAnalyser implements Analyzer
{
    /** @var UuidFactoryInterface */
    private $uuidFactory;

    /** @var Tokenizer */
    private $tokenizer;

    public function __construct(UuidFactoryInterface $uuidFactory, Tokenizer $tokenizer)
    {
        $this->uuidFactory = $uuidFactory;
        $this->tokenizer = $tokenizer;
    }

    public function process(Document $document): DocumentAnalysis
    {
        $uuid = $this->uuidFactory->uuid1();

        $report = new Report($this->reportHeaders(), $this->analyse($document));

        return new KeywordsDocumentAnalysis($uuid, $document, $report);
    }

    private function reportHeaders(): array
    {
        return [
            KeywordsDocumentAnalysis::COLUMN_KEYWORD     => trans('Keyword'),
            KeywordsDocumentAnalysis::COLUMN_OCCURRENCES => trans('Number of occurrences'),
        ];
    }

    private function analyse(Document $document): array
    {
        $reportData = $this->computeResult($document);

        $this->sort($reportData);

        return array_values($reportData);
    }

    private function computeResult(Document $document): array
    {
        $reportData = $this->makeEmptyResult($document);

        foreach ($this->tokenizer->tokens($document->text(), Document::WORD_DELIMITER) as $word) {
            if (array_key_exists($word, $reportData)) {
                $reportData[$word][KeywordsDocumentAnalysis::COLUMN_OCCURRENCES]++;
            }
        }

        return $reportData;
    }

    private function makeEmptyResult(Document $document): array
    {
        foreach ($document->metadata()->keywords() as $keyword => $keywordName) {
            $keywords[$keyword] = [
                KeywordsDocumentAnalysis::COLUMN_KEYWORD     => $keywordName,
                KeywordsDocumentAnalysis::COLUMN_OCCURRENCES => 0,
            ];
        }

        return $keywords ?? [];
    }

    private function sort(array &$reportData)
    {
        uasort(
            $reportData,
            function (array $a, array $b) {
                if ($a[KeywordsDocumentAnalysis::COLUMN_OCCURRENCES] === $b[KeywordsDocumentAnalysis::COLUMN_OCCURRENCES]) {
                    return $a[KeywordsDocumentAnalysis::COLUMN_KEYWORD] <=> $b[KeywordsDocumentAnalysis::COLUMN_KEYWORD];
                }

                return $b[KeywordsDocumentAnalysis::COLUMN_OCCURRENCES] <=> $a[KeywordsDocumentAnalysis::COLUMN_OCCURRENCES];
            }
        );
    }
}

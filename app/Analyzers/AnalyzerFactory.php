<?php namespace App\Analyzers;

use App\Analyses\AnalysisType;
use Illuminate\Foundation\Application;

class AnalyzerFactory
{
    /** @var array */
    private $analyzers = [
        AnalysisType::KEYWORDS => KeywordsAnalyser::class,
    ];

    /** @var Application */
    private $application;

    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    public function make(string $type): Analyzer
    {
        if (!isset($this->analyzers[$type])) {
            throw new \RuntimeException('Unknown analyzer type: ' . $type);
        }

        return $this->application->make($this->analyzers[$type]);
    }

    public function registerAnalyzer(string $type, string $implementation)
    {
        $this->validateImplementation($implementation);

        $this->analyzers[$type] = $implementation;
    }

    private function validateImplementation(string $implementation)
    {
        if (!class_exists($implementation)) {
            throw new \RuntimeException('Class not found: ' . $implementation);
        }

        if (!in_array(Analyzer::class, class_implements($implementation))) {
            throw new \RuntimeException('Class does not implement Analyzer interface: ' . $implementation);
        }
    }
}

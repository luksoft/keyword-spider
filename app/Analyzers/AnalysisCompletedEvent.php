<?php

namespace App\Analyzers;

use App\Analyses\DocumentAnalysis;

class AnalysisCompletedEvent
{
    /** @var DocumentAnalysis */
    private $analysis;

    public function __construct(DocumentAnalysis $analysis)
    {
        $this->analysis = $analysis;
    }

    public function analysis(): DocumentAnalysis
    {
        return $this->analysis;
    }
}

<?php namespace App\Analyzers;

use App\Analyses\DocumentAnalysis;
use App\Document\Document;

interface Analyzer
{
    public function process(Document $document): DocumentAnalysis;
}

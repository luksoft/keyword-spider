<?php namespace App\Report;

class Report
{
    /** @var array */
    private $header;

    /** @var array */
    private $data;

    /**
     * Report constructor.
     * @param array $header
     * @param array $data
     */
    public function __construct(array $header, array $data)
    {
        $this->header = $header;
        $this->data = $data;
    }

    public function header(): array
    {
        return $this->header;
    }

    public function data(): array
    {
        return $this->data;
    }
}

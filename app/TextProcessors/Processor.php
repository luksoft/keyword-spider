<?php namespace App\TextProcessors;

interface Processor
{
    public function process(string $text);
}

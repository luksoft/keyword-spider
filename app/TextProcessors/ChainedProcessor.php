<?php namespace App\TextProcessors;

abstract class ChainedProcessor implements Processor
{
    /** @var Processor */
    private $nextStep;

    abstract protected function filter(string $text): string;

    public function process(string $text): string
    {
        if ($this->nextStep) {
            return $this->nextStep->process($this->filter($text));

        } else {
            return $this->filter($text);
        }
    }

    public function setNextStep(ChainedProcessor $processor): ChainedProcessor
    {
        $this->nextStep = $processor;

        return $this->nextStep;
    }
}

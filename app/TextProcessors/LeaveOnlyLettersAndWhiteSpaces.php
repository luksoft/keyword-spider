<?php namespace App\TextProcessors;

class LeaveOnlyLettersAndWhiteSpaces extends ChainedProcessor
{
    protected function filter(string $text): string
    {
        return preg_replace("/[^[:alnum:][:space:]]/u", '', $text);
    }
}

<?php namespace App\TextProcessors;

class TrimSpaces extends ChainedProcessor
{
    protected function filter(string $text): string
    {
        return trim($text);
    }
}

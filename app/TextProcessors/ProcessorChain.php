<?php namespace App\TextProcessors;

class ProcessorChain implements Processor
{
    /** @var ChainedProcessor */
    private $head;

    /** @var ChainedProcessor */
    private $current;

    public function addChainLink(ChainedProcessor $processor): ProcessorChain
    {
        if (empty($this->head)) {
            return $this->setHead($processor);
        }

        $this->current = $this->current->setNextStep($processor);

        return $this;
    }

    public function process(string $text): string
    {
        return $this->head->process($text);
    }

    private function setHead(ChainedProcessor $processor)
    {
        $this->head = $this->current = $processor;

        return $this;
    }
}

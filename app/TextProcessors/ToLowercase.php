<?php namespace App\TextProcessors;

use App\Encoding\EncodingNormalizer;

class ToLowercase extends ChainedProcessor
{
    protected function filter(string $text): string
    {
        return mb_strtolower($text, EncodingNormalizer::TARGET_ENCODING);
    }
}

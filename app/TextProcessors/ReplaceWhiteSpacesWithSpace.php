<?php namespace App\TextProcessors;

use App\Document\Document;

class ReplaceWhiteSpacesWithSpace extends ChainedProcessor
{
    protected function filter(string $text): string
    {
        return preg_replace("/[[:space:]]+/u", Document::WORD_DELIMITER, $text);
    }
}

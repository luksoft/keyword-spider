<?php namespace App\Jobs;

use Carbon\Carbon;

class FailedJob
{
    /** @var string */
    private $description;

    /** @var string */
    private $errorMessage;

    /** @var Carbon */
    private $created;

    public function __construct(string $description, string $errorMessage)
    {
        $this->description = $description;
        $this->errorMessage = $errorMessage;
        $this->created = Carbon::now();
    }

    public function description(): string
    {
        return $this->description;
    }

    public function errorMessage(): string
    {
        return $this->errorMessage;
    }

    public function created(): Carbon
    {
        return $this->created;
    }
}

<?php

namespace App\Jobs;

class JobFailedEvent
{
    /** @var \Exception */
    private $exception;

    /** @var string */
    private $description;

    public function __construct(string $description, \Exception $exception)
    {
        $this->description = $description;
        $this->exception = $exception;
    }

    /** @return string */
    public function description(): string
    {
        return $this->description;
    }

    /** @return \Exception */
    public function exception(): \Exception
    {
        return $this->exception;
    }
}

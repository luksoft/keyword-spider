<?php namespace App\Jobs\Repository;

use App\Jobs\FailedJob;
use App\Storage\FixedCircularArray;
use Illuminate\Cache\Repository;

/**
 * Simple implementation using cache storage, not safe for concurrent usage.
 * It stores fixed number of events, oldest are lost if the buffer is full.
 * Do not use in production!!!
 */
class CacheFailedJobsRepository implements FailedJobsRepository
{
    const COLLECTION_KEY = 'failed-jobs-collection';
    const COLLECTION_SIZE = 20;

    /** @var Repository */
    private $storage;

    /** @var FixedCircularArray */
    private $collection;

    /** @var int */
    private $size;

    public function __construct(Repository $storage, int $size = self::COLLECTION_SIZE)
    {
        $this->storage = $storage;
        $this->size = $size;
    }

    public function store(FailedJob $failedJob)
    {
        $this->loadCollection();

        $this->collection->push($failedJob);

        $this->storeCollection();
    }

    public function all(): \Generator
    {
        $this->loadCollection();

        return $this->collection->all();
    }

    private function loadCollection()
    {
        $this->collection = $this->storage->get(
            static::COLLECTION_KEY,
            function () {
                return new FixedCircularArray($this->size);
            }
        );
    }

    private function storeCollection()
    {
        $this->storage->forever(static::COLLECTION_KEY, $this->collection);
    }
}

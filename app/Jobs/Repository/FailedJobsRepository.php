<?php namespace App\Jobs\Repository;

use App\Jobs\FailedJob;

interface FailedJobsRepository
{
    public function store(FailedJob $failedJob);
    public function all(): \Generator;
}

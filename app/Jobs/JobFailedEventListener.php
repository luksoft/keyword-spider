<?php namespace App\Jobs;

use App\Jobs\Repository\FailedJobsRepository;

class JobFailedEventListener
{
    /** @var FailedJobsRepository */
    private $failedJobsRepository;

    public function __construct(FailedJobsRepository $failedJobsRepository)
    {
        $this->failedJobsRepository = $failedJobsRepository;
    }

    public function handle(JobFailedEvent $jobFailedEvent)
    {
        $failedJob = new FailedJob($jobFailedEvent->description(), $jobFailedEvent->exception()->getMessage());

        $this->failedJobsRepository->store($failedJob);
    }
}

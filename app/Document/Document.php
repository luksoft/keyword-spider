<?php namespace App\Document;

use Ramsey\Uuid\UuidInterface;

class Document
{
    const WORD_DELIMITER = ' ';

    /** @var UuidInterface */
    private $uuid;

    /** @var string */
    private $name;

    /** @var string */
    private $text;

    /** @var DocumentMetadata */
    private $metadata;

    /**
     * @param UuidInterface    $uuid
     * @param string           $name
     * @param string           $text
     * @param DocumentMetadata $metadata
     */
    public function __construct(UuidInterface $uuid, string $name, string $text, DocumentMetadata $metadata)
    {
        $this->uuid = $uuid;
        $this->name = $name;
        $this->text = $text;
        $this->metadata = $metadata;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function text(): string
    {
        return $this->text;
    }

    public function metadata(): DocumentMetadata
    {
        return $this->metadata;
    }
}

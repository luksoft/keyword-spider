<?php namespace App\Document;

class DocumentMetadata
{
    /** @var array */
    private $keywords;

    /**
     * @param array $keywords
     */
    public function __construct(array $keywords = [])
    {
        $this->keywords = $keywords;
    }

    public function keywords(): array
    {
        return $this->keywords;
    }
}

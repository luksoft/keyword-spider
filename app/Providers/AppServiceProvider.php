<?php

namespace App\Providers;

use App\Analyses\Repository\AnalysesRepository;
use App\Analyses\Repository\CacheAnalysesRepository;
use App\Encoding\EncodingNormalizer;
use App\Encoding\MbEncodingNormalizer;
use App\HtmlDocument\Creators\HtmlDocumentCreator;
use App\HtmlDocument\Creators\WebPageHtmlDocumentCreator;
use App\HtmlDocument\Parsers\DomHtmlParser;
use App\HtmlDocument\Parsers\HtmlParser;
use App\HtmlDocument\Parsers\HtmlToTextParser;
use App\HtmlDocument\Parsers\NativeHtmlToTextParser;
use App\Jobs\Repository\CacheFailedJobsRepository;
use App\Jobs\Repository\FailedJobsRepository;
use App\Tokenizers\StrTokTokenizer;
use App\Tokenizers\Tokenizer;
use App\WebPage\Providers\GuzzleWebPageProvider;
use App\WebPage\Providers\WebPageProvider;
use Illuminate\Support\ServiceProvider;
use Ramsey\Uuid\UuidFactory;
use Ramsey\Uuid\UuidFactoryInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UuidFactoryInterface::class, UuidFactory::class);
        $this->app->bind(HtmlDocumentCreator::class, WebPageHtmlDocumentCreator::class);
        $this->app->bind(WebPageProvider::class, GuzzleWebPageProvider::class);
        $this->app->bind(HtmlParser::class, DomHtmlParser::class);
        $this->app->bind(EncodingNormalizer::class, MbEncodingNormalizer::class);
        $this->app->bind(HtmlToTextParser::class, NativeHtmlToTextParser::class);
        $this->app->bind(AnalysesRepository::class, CacheAnalysesRepository::class);
        $this->app->bind(Tokenizer::class, StrTokTokenizer::class);
        $this->app->bind(FailedJobsRepository::class, CacheFailedJobsRepository::class);
    }
}

<?php namespace App\HtmlDocument;

use App\Analyzers\AnalysisCompletedEvent;
use App\Analyzers\AnalyzerFactory;
use App\Jobs\JobFailedEvent;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Queue\ShouldQueue;
use Ramsey\Uuid\UuidInterface;

class AnalyseWebPageJob implements ShouldQueue
{
    /** @var UuidInterface */
    private $uuid;

    /** @var string */
    private $url;

    /** @var string */
    private $analysis;

    public function __construct(UuidInterface $uuid, string $url, string $analysis)
    {
        $this->uuid = $uuid;
        $this->url = $url;
        $this->analysis = $analysis;
    }

    public function handle(
        DocumentCreator $documentCreator,
        AnalyzerFactory $analyzerFactory,
        Dispatcher $eventDispatcher
    ) {
        try{
            $document = $documentCreator->make($this->url);

            $analysis = $analyzerFactory->make($this->analysis)->process($document);

            $eventDispatcher->dispatch(new AnalysisCompletedEvent($analysis));
        }
        catch (\Exception $e){
            $eventDispatcher->dispatch(new JobFailedEvent($this->makeDescription(), $e));
        }
    }

    private function makeDescription()
    {
        return sprintf('"%1$s" analysis of page %2$s', ucfirst($this->analysis), $this->url);
    }
}

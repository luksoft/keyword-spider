<?php namespace App\HtmlDocument\Parsers;

interface HtmlParser
{
    const KEYWORDS_DELIMITER = ',';

    public function load(string &$html);
    public function keywords(): array;
    public function charset(): string;
    public function body(): string;
}

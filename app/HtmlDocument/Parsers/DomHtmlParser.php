<?php namespace App\HtmlDocument\Parsers;

use App\Encoding\EncodingNormalizer;
use App\HtmlDocument\KeywordsNormalizer;
use App\Tokenizers\Tokenizer;

class DomHtmlParser implements HtmlParser
{
    /** @var \DOMDocument */
    private $dom;

    /** @var bool */
    private $initialized;

    /** @var bool */
    private $removeScripts;

    /** @var Tokenizer */
    private $tokenizer;

    /** @var KeywordsNormalizer */
    private $keywordsNormalizer;
    /**
     * @var EncodingNormalizer
     */
    private $encodingNormalizer;

    public function __construct(
        Tokenizer $tokenizer,
        KeywordsNormalizer $keywordsNormalizer,
        EncodingNormalizer $encodingNormalizer,
        bool $removeScripts = true
    ) {
        $this->tokenizer = $tokenizer;
        $this->keywordsNormalizer = $keywordsNormalizer;
        $this->encodingNormalizer = $encodingNormalizer;
        $this->removeScripts = $removeScripts;
        $this->initialized = false;

        $this->dom = new \DOMDocument();
    }

    public function load(string &$html)
    {
        $this->loadDom($html);

        $this->removeScripts();

        $this->initialized = true;
    }

    public function keywords(): array
    {
        $this->ensureIsInitialized();

        $keywordsList = $this->findMetaNodeContentByName('keywords');

        return $this->tokenizeKeywords($keywordsList);
    }

    public function charset(): string
    {
        $this->ensureIsInitialized();

        return $this->dom->encoding;
    }

    public function body(): string
    {
        $this->ensureIsInitialized();

        return $this->dom->saveHTML(
            $this->dom->getElementsByTagName('body')->item(0)
        );
    }

    private function loadDom(string &$html)
    {
        libxml_use_internal_errors(true);

        $this->normalizeEncoding($html);

        if (!$this->dom->loadHTML($html)) {
            throw new \RuntimeException('Cannot parse document');
        }

        $this->dom->encoding = EncodingNormalizer::TARGET_ENCODING;

        libxml_use_internal_errors(false);
    }

    private function normalizeEncoding(string &$html)
    {
        $sourceEncoding = $this->findSourceEncoding($html);
        $entitiesEncoding = EncodingNormalizer::ENTITIES_ENCODING;

        $html = $this->encodingNormalizer->process($html, $sourceEncoding, $entitiesEncoding);
    }

    private function findSourceEncoding(string &$html): string
    {
        $dom = new \DOMDocument();
        $dom->loadHTML($html);

        return $dom->encoding;
    }

    private function ensureIsInitialized()
    {
        if (!$this->initialized) {
            throw new \RuntimeException('Parser is not initialized');
        }
    }

    private function findMetaNodeContentByName($name): string
    {
        /** @var \DOMElement $node */
        foreach ($this->dom->getElementsByTagName('meta') as $node) {
            if (strtolower($node->getAttribute('name')) === $name) {
                return $node->getAttribute('content');
            }
        }

        return '';
    }

    private function tokenizeKeywords(string $keywordsList): array
    {
        $keywords = [];

        foreach ($this->tokenizer->tokens($keywordsList, HtmlParser::KEYWORDS_DELIMITER) as $keyword) {
            $keywords[$this->keywordsNormalizer->process($keyword)] = $keyword;
        }

        return $keywords;
    }

    private function removeScripts()
    {
        if (!$this->removeScripts) {
            return;
        }

        /** @var \DOMElement $node */
        foreach (iterator_to_array($this->dom->getElementsByTagName('script')) as $node) {
            $node->parentNode->removeChild($node);
        }
    }
}

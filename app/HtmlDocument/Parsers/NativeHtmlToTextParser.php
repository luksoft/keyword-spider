<?php namespace App\HtmlDocument\Parsers;

use App\HtmlDocument\TextProcessors\DecodeHtmlEntities;
use App\HtmlDocument\TextProcessors\DecodeHtmlNumericUnicodeEntities;
use App\HtmlDocument\TextProcessors\StripHtml;
use App\TextProcessors\LeaveOnlyLettersAndWhiteSpaces;
use App\TextProcessors\ProcessorChain;
use App\TextProcessors\ReplaceWhiteSpacesWithSpace;
use App\TextProcessors\ToLowercase;

class NativeHtmlToTextParser implements HtmlToTextParser
{
    /** @var ProcessorChain */
    private $processor;

    public function __construct()
    {
        $this->processor = (new ProcessorChain())
            ->addChainLink(new StripHtml())
            ->addChainLink(new DecodeHtmlEntities())
            ->addChainLink(new DecodeHtmlNumericUnicodeEntities())
            ->addChainLink(new LeaveOnlyLettersAndWhiteSpaces())
            ->addChainLink(new ReplaceWhiteSpacesWithSpace())
            ->addChainLink(new ToLowercase());
    }

    public function parse(string $html): string
    {
        return $this->processor->process($html);
    }
}

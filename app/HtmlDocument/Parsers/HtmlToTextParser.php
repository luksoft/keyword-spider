<?php namespace App\HtmlDocument\Parsers;

interface HtmlToTextParser
{
    public function parse(string $html): string;
}

<?php namespace App\HtmlDocument\TextProcessors;

use App\Encoding\EncodingNormalizer;
use App\TextProcessors\ChainedProcessor;

class DecodeHtmlNumericUnicodeEntities extends ChainedProcessor
{
    const CONVMAP = [0x0, 0x2FFFF, 0, 0xFFFF];

    protected function filter(string $text): string
    {
        return mb_decode_numericentity($text, self::CONVMAP, EncodingNormalizer::TARGET_ENCODING);
    }
}

<?php namespace App\HtmlDocument\TextProcessors;

use App\Encoding\EncodingNormalizer;
use App\TextProcessors\ChainedProcessor;

class DecodeHtmlEntities extends ChainedProcessor
{
    protected function filter(string $text): string
    {
        return html_entity_decode($text, ENT_QUOTES, EncodingNormalizer::TARGET_ENCODING);
    }
}

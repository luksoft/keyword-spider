<?php namespace App\HtmlDocument\TextProcessors;

use App\TextProcessors\ChainedProcessor;

class StripHtml extends ChainedProcessor
{
    protected function filter(string $text): string
    {
        return strip_tags($text);
    }
}

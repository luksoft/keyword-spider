<?php namespace App\HtmlDocument;

use App\HtmlDocument\TextProcessors\DecodeHtmlEntities;
use App\HtmlDocument\TextProcessors\DecodeHtmlNumericUnicodeEntities;
use App\TextProcessors\LeaveOnlyLettersAndWhiteSpaces;
use App\TextProcessors\ProcessorChain;
use App\TextProcessors\ReplaceWhiteSpacesWithSpace;
use App\TextProcessors\ToLowercase;
use App\TextProcessors\TrimSpaces;

class KeywordsNormalizer
{
    /** @var ProcessorChain */
    private $processor;

    public function __construct()
    {
        $this->processor = (new ProcessorChain())
            ->addChainLink(new DecodeHtmlEntities())
            ->addChainLink(new DecodeHtmlNumericUnicodeEntities())
            ->addChainLink(new LeaveOnlyLettersAndWhiteSpaces())
            ->addChainLink(new ReplaceWhiteSpacesWithSpace())
            ->addChainLink(new ToLowercase())
            ->addChainLink(new TrimSpaces());
    }

    public function process(string $keyword): string
    {
        return $this->processor->process($keyword);
    }
}

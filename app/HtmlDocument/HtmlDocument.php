<?php namespace App\HtmlDocument;

class HtmlDocument
{
    private $keywords;
    private $body;

    public function __construct(array $keywords, string $body)
    {
        $this->keywords = $keywords;
        $this->body = $body;
    }

    public function keywords(): array
    {
        return $this->keywords;
    }

    public function body(): string
    {
        return $this->body;
    }
}

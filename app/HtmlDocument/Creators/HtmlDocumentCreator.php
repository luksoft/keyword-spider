<?php namespace App\HtmlDocument\Creators;

use App\HtmlDocument\HtmlDocument;

interface HtmlDocumentCreator
{
    public function create(string $source): HtmlDocument;
}

<?php namespace App\HtmlDocument\Creators;

use App\Encoding\EncodingNormalizer;
use App\HtmlDocument\HtmlDocument;
use App\HtmlDocument\Parsers\HtmlParser;
use App\WebPage\Providers\WebPageProvider;

class WebPageHtmlDocumentCreator implements HtmlDocumentCreator
{
    /** @var WebPageProvider */
    private $webPageProvider;

    /** @var HtmlParser */
    private $htmlParser;

    /** @var EncodingNormalizer */
    private $encodingNormalizer;

    public function __construct(
        WebPageProvider $webPageProvider,
        HtmlParser $htmlParser,
        EncodingNormalizer $encodingNormalizer
    ) {
        $this->webPageProvider = $webPageProvider;
        $this->htmlParser = $htmlParser;
        $this->encodingNormalizer = $encodingNormalizer;
    }

    public function create(string $source): HtmlDocument
    {
        $response = $this->webPageProvider->getFromUrl($source)->getBody();

        $html = $response->getContents();

        $this->htmlParser->load($html);

        return new HtmlDocument($this->htmlParser->keywords(), $this->htmlParser->body());
    }
}

<?php namespace App\HtmlDocument;

use App\Document\Document;
use App\Document\DocumentMetadata;
use App\HtmlDocument\Creators\HtmlDocumentCreator;
use App\HtmlDocument\Parsers\HtmlToTextParser;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidFactoryInterface;

class DocumentCreator
{
    /** @var UuidFactoryInterface */
    private $uuidFactory;

    /** @var HtmlDocumentCreator */
    private $htmlDocumentCreator;

    /** @var HtmlToTextParser */
    private $htmlToTextParser;

    public function __construct(
        UuidFactoryInterface $uuidFactory,
        HtmlDocumentCreator $htmlDocumentCreator,
        HtmlToTextParser $htmlToTextParser
    ) {
        $this->uuidFactory = $uuidFactory;
        $this->htmlDocumentCreator = $htmlDocumentCreator;
        $this->htmlToTextParser = $htmlToTextParser;
    }

    public function make($url): Document
    {
        $htmlDocument = $this->htmlDocumentCreator->create($url);

        $uuid = $this->uuidFactory->uuid5(Uuid::NAMESPACE_URL, $url);
        $text = $this->htmlToTextParser->parse($htmlDocument->body());
        $metadata = new DocumentMetadata($htmlDocument->keywords());

        return new Document($uuid, $url, $text, $metadata);
    }
}

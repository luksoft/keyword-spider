<?php namespace App\WebPage;

use App\WebPage\Validators\UrlValidator;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\UriInterface;

class UriProvider
{
    /** @var UrlValidator */
    private $urlValidator;

    public function __construct(UrlValidator $urlValidator)
    {
        $this->urlValidator = $urlValidator;
    }

    public function makeFromString(string $url): UriInterface
    {
        $url = filter_var($url, FILTER_SANITIZE_URL);

        $this->urlValidator->validate($url);

        return new Uri($url);
    }
}

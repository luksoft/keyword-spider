<?php namespace App\WebPage\Providers;

use App\WebPage\UriProvider;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use Psr\Http\Message\ResponseInterface;

class GuzzleWebPageProvider implements WebPageProvider
{
    /** @var Client */
    private $client;

    /** @var UriProvider */
    private $uriProvider;

    public function __construct(Client $client, UriProvider $uriProvider)
    {
        $this->client = $client;
        $this->uriProvider = $uriProvider;
    }

    /** @inheritdoc */
    public function getFromUrl(string $url): ResponseInterface
    {
        $uri = $this->uriProvider->makeFromString($url);

        try {
            return $this->client->get($uri);
        } catch (TransferException $exception) {
            throw new WebPageProviderException($exception->getMessage());
        }
    }
}

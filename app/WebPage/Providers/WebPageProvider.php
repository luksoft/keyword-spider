<?php namespace App\WebPage\Providers;

use Psr\Http\Message\ResponseInterface;

interface WebPageProvider
{
    /**
     * @param string $url
     * @return ResponseInterface
     * @throws WebPageProviderException
     */
    public function getFromUrl(string $url): ResponseInterface;
}

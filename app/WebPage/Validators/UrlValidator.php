<?php namespace App\WebPage\Validators;

use App\Exceptions\ValidationException;

class UrlValidator
{
    public function validate(string $url)
    {
        $errors = [];

        $valid = filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED | FILTER_FLAG_HOST_REQUIRED);

        if (!$valid) {
            $errors[] = 'Invalid URL';
        }

        $urlComponents = parse_url($url);

        if (!in_array($urlComponents['scheme'], ['http', 'https'])) {
            $errors[] = 'Only http or https scheme are supported';
        }

        if (!empty($errors)) {
            throw new ValidationException($errors);
        }
    }
}

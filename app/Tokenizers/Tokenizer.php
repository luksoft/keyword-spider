<?php namespace App\Tokenizers;

interface Tokenizer
{
    public function tokens(string $text, string $delimiter): \Generator;
}

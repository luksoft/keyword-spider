<?php namespace App\Tokenizers;

class StrTokTokenizer implements Tokenizer
{
    public function tokens(string $text, string $delimiter): \Generator
    {
        $token = strtok($text, $delimiter);

        if ($token !== false) {
            yield $token;
        }

        while ($token !== false) {
            $token = strtok($delimiter);

            if ($token !== false) {
                yield $token;
            }
        }
    }
}

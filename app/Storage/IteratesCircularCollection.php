<?php namespace App\Storage;

trait IteratesCircularCollection
{
    private function allValues(\SplFixedArray $store, int $position, int $size): \Generator
    {
        $position = ($position ?: $size) - 1;

        for ($i = 1, $len = $size; $i <= $len; $i++) {
            $value = $store[$position];

            if (is_null($value)) {
                break;
            }

            yield $value;

            $position = ($position ?: $size) - 1;
        }
    }
}

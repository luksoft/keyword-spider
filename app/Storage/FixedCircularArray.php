<?php namespace App\Storage;

class FixedCircularArray
{
    use IteratesCircularCollection;

    /** @var \SplFixedArray */
    private $store;

    /** @var int */
    private $position;

    /** @var int */
    private $size;

    public function __construct(int $size)
    {
        if ($size <= 0) {
            throw new \RuntimeException('O size list not supported');
        }

        $this->store = new \SplFixedArray($size);
        $this->position = 0;
        $this->size = $size;
    }

    public function push($value)
    {
        if (is_null($value)) {
            throw new \RuntimeException('Null values are not supported');
        }

        $this->store[$this->position] = $value;

        $this->incrementPosition();
    }

    public function all(): \Generator
    {
        return $this->allValues($this->store, $this->position, $this->size);
    }

    private function incrementPosition()
    {
        $this->position = ($this->position + 1) % $this->size;
    }
}

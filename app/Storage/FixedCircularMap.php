<?php namespace App\Storage;

class FixedCircularMap
{
    use IteratesCircularCollection;

    /** @var \SplFixedArray */
    private $store;

    /** @var \SplFixedArray */
    private $keys;

    /** @var int */
    private $position;

    /** @var int */
    private $size;

    public function __construct(int $size)
    {
        if ($size <= 0) {
            throw new \RuntimeException('O size list not supported');
        }

        $this->store = new \SplFixedArray($size);
        $this->keys = new \SplFixedArray($size);
        $this->position = 0;
        $this->size = $size;
    }

    public function set($key, $value)
    {
        if (is_null($value)) {
            throw new \RuntimeException('Null values are not supported');
        }

        $this->store[$this->position] = $value;
        $this->keys[$this->position] = $key;

        $this->incrementPosition();
    }

    public function get($key, $default = null)
    {
        $position = $this->findKeyPosition($key);
        if ($position === false) {
            return $default;
        }

        return $this->store[$position];
    }

    public function contains($key): bool
    {
        return $this->findKeyPosition($key) !== false;
    }

    public function all(): \Generator
    {
        return $this->allValues($this->store, $this->position, $this->size);
    }

    private function findKeyPosition($key)
    {
        return array_search($key, $this->keys->toArray());
    }

    private function incrementPosition()
    {
        $this->position = ($this->position + 1) % $this->size;
    }
}

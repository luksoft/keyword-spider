<?php namespace App\Encoding;

interface EncodingNormalizer
{
    const TARGET_ENCODING = 'UTF-8';
    const ENTITIES_ENCODING = 'HTML-ENTITIES';

    public function process(string $text, string $fromEncoding, string $toEncoding = self::TARGET_ENCODING);
}

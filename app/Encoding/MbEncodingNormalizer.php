<?php namespace App\Encoding;

class MbEncodingNormalizer implements EncodingNormalizer
{
    private $supportedEncodings;

    public function __construct()
    {
        $this->supportedEncodings = array_map([$this, 'normalize'], mb_list_encodings());
    }

    public function process(string $text, string $fromEncoding, string $toEncoding = self::TARGET_ENCODING)
    {
        if ($this->sameEncoding($fromEncoding)) {
            return $text;
        }

        $this->validate($fromEncoding);

        return mb_convert_encoding($text, $toEncoding, $fromEncoding);
    }

    private function normalize(string $value): string
    {
        return strtolower($value);
    }

    private function validate(string $fromEncoding)
    {
        if (!in_array($this->normalize($fromEncoding), $this->supportedEncodings)) {
            throw new \RuntimeException('Not supported encoding: ' . $fromEncoding);
        }
    }

    private function sameEncoding(string $fromEncoding): bool
    {
        return $this->normalize($fromEncoding) === $this->normalize(EncodingNormalizer::TARGET_ENCODING);
    }
}
